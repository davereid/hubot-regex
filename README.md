hubot-regex
===========

Hubot script that allows users to perform "s/find/replace/" regexes on their
own or all messages in a chat.

This script does _not_ actually use the sed command to perform the replacement.
It uses the JavaScript PCRE regex replacement, to make things simpler and not
potentially open any security risks due to running local commands.

## Installing

1. Add `hubot-regex` as a dependency to your hubot:
    `"hubot-regex": ""`
2. Tell hubot to load it. Add `"hubot-regex"` to the `external-scripts.json`
   list in the hubot root folder.
3. Run `npm install` while you grab a beer.

## Configuration

By default, when a user performs a search-replace, this script will only
perform replacement on the last message that matches the same user in the same
room. If you want to make the script perform replacement on any last message in
the room (not restricted by the same user), then add the following
configuration:

* `HUBOT_REGEX_GLOBAL="true"`
  Set to true to make search-replacement work across all users in the room.
* (Optional) `HUBOT_REGEX_GLOBAL_LIMIT=[integer]`
  Limits the number of lines in the room to use for search-replacement.
